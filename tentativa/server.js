const dotenv = require('dotenv');
const express = require('express');
const bodyParser = require('body-parser');
const authMiddleware = require('./middleware/auth');
const privateRoutes = require('./routes/private');
const publicRoutes = require('./routes/public');
const { buildSchema } = require('graphql');
const graphqlHTTP = require('express-graphql');
const prestadores = require('./data/prestadores');

dotenv.config();
const app = express();

const schema = buildSchema(
    `type Query {
        user (id:String): String
        prestador(nif:String): Prestador
        allPrestadores:[Prestador]
    }
    type Prestador{
        id:ID,
        name:String,
        locais:[Prestador!]
    }
   
`);


const resolvers = {
    user: (args, context, info) => {
        console.log(context.headers);
        return args.id;
    },
    allPrestadores: () => prestadores.allIDs.map(pID => prestadores.prestadoresByNif[pID]),
    prestador : (args, context) => {
        const { nif } = args;
        return prestadores.prestadoresByNif[nif];
            }
};


//app.use(bodyParser.json());
app.use('/', graphqlHTTP({
    schema: schema,
    rootValue: resolvers,
    //context: {
    //    //dbConnect: sequelizeConnect,
    //    req:req
    //},
    graphiql: true
}));
//app.use('/api/protected', authMiddleware, privateRoutes);
//app.use('/api', publicRoutes);

app.listen(8081);