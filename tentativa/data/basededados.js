﻿'use strict';
var http = require('http');
var port = process.env.PORT || 1337;
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'mySQL',
    database: 'northwind'
});

connection.connect(function (err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

connection.query('SELECT * FROM customers', function (error, results, fields) {
    if (error) throw error;
    console.log('The solution is: ', results[0]);
    http.createServer(function (req, res) {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(results));
    }).listen(port);

});

connection.end();